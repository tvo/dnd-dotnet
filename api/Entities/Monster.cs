namespace api.Entities;

public class Monster : IHaveId
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
	public string Link { get; set; }
	public float CR { get; set; }
	public Stats Stats { get; set; }
	public CreatureStats CreatureStats { get; set; }
	public string AttackDescription { get; set; }
	public string LegendaryDescription { get; set; }
	public int LegendaryActions { get; set; }
	public string SourceBook { get; set; }
	public List<string> Actions { get; set; }
	public List<string> Languages { get; set; }
	public string Environment { get; set; }
}
