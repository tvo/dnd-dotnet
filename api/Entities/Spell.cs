namespace api.Entities;

public class Spell : IHaveId
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public List<string> Class { get; set; }
    public string School { get; set; }
    public bool Concentration { get; set; }
    public int Level { get; set; }
    public float AverageDamage { get; set; } = 0;
    public string Components { get; set; } = null;
}
