using Microsoft.EntityFrameworkCore;

[Owned]
public class StatsCore
{
	public int Con { get; set; } 
	public int Str { get; set; }
	public int Dex { get; set; }
	public int Int { get; set; }
	public int Wis { get; set; }
	public int Cha { get; set; }
};

public enum Conditions {
	Blinded,
	Charmed,
	Deafened,
	Frightened,
	Grappled,
	Incapacitated,
	Invisible,
	Paralyzed,
	Petrified,
	Poisoned,
	Prone,
	Restrained,
	Stunned,
	Unconscious,
	Exhaustion,
}

[Owned]
public class Skills
{
	// Str
	public int Athletics { get; set; }
	// Dex
	public int Acrobatics { get; set; }
	public int SlightOfHand { get; set; }
	public int Stealth { get; set; }
	// Int
	public int Arcana { get; set; }
	public int History { get; set; }
	public int Investigation { get; set; }
	public int Nature { get; set; }
	public int Religion { get; set; }
	// Wis
	public int AnimalHandling { get; set; }
	public int Insight { get; set; }
	public int Medicine { get; set; }
	public int Perception { get; set; }
	public int Survival { get; set; }
	// Cha
	public int Deception { get; set; }
	public int Intimidation { get; set; }
	public int Performance { get; set; }
	public int Persuasion { get; set; }
}

[Owned]
public class Speeds
{
	public int Walking { get; set; }
	public int Swimming { get; set; }
	public int Flying { get; set; }
	public int Climbing { get; set; }
	public int Burrowing { get; set; }
}

[Owned]
public class Vision
{
	public int DarkVision { get; set; }
	public int BlindSight { get; set; }
	public int TrueSight { get; set; }
}

[Owned]
public class CreatureStats
{
	public int Initiative { get; set; }
	public int Proficiency { get; set; }
	public Speeds Speed { get; set; }
	public Vision Vision { get; set; }
	public string Size { get; set; }
	public string Type { get; set; }
	public List<Conditions> ConditionImmunities { get; set; } = new();
}

[Owned]
public class Stats
{
	public int Hp { get; set; }
	public int Ac { get; set; }
	public int Proficiency { get; set; }
	public StatsCore PrimaryStats { get; set; }
	public StatsCore AbilityBonus { get; set; }
	public StatsCore SavingThrowBonus { get; set; }
	public Skills SkillBonus { get; set; }
	public string SpellCastingStat { get; set; }
};
