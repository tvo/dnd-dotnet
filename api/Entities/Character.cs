namespace api.Entities;

public class Character : IHaveId
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int Edition { get; set; }
}
