using System;
using api.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Newtonsoft.Json;

namespace api.db;


public class DataContext : DbContext
{
	public DataContext(DbContextOptions<DataContext> options) : base(options)
	{
	}

	protected override void OnModelCreating(ModelBuilder builder)
	{
		builder.Entity<Spell>().Property(e => e.Class) .HasConversion(v => JsonConvert.SerializeObject(v), v => JsonConvert.DeserializeObject<List<string>>(v),
				new ValueComparer<List<string>>(
					(c1, c2) => c1.SequenceEqual(c2),
					c => c.Aggregate(0, (a, v) => HashCode.Combine(a, v.GetHashCode())),
					c => c.ToList())
				);
		builder.Entity<Spell>(x => x.HasIndex(s => s.Name).IsUnique());
		builder.Entity<FiveEClass>();
		builder.Entity<Character>();
		builder.Entity<Monster>().OwnsOne(x => x.Stats);
		builder.Entity<Monster>().OwnsOne(x => x.CreatureStats);
		builder.Entity<Monster>().OwnsOne(x => x.Actions);
		builder.Entity<Monster>().OwnsOne(x => x.Languages);
	}

	public DbSet<Spell> Spell { get; set; }
	public DbSet<FiveEClass> Classes { get; set; }
	public DbSet<Character> Characters { get; set; }
	public DbSet<Monster> Monsters { get; set; }
}

