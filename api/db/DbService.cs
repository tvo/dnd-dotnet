using System;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;

namespace api.db;

public class DbService : IHostedService
{
    private readonly ILogger logger;
    private readonly DataContext context;

    public DbService(ILogger<DbService> logger, DataContext context)
    {
        this.logger = logger;
        this.context = context;
    }


    public async Task StartAsync(CancellationToken cancel)
    {
        try
        {
            logger.LogInformation("Starting database migration.");
            await context.Database.MigrateAsync();
            logger.LogInformation("Finished database migration");
        }
        catch (Exception ex)
        {
            logger.LogError("Failed to initialize the database. Error {Error}.", ex.Message);
        }
    }

    public Task StopAsync(CancellationToken cancel)
    {
        return Task.CompletedTask;
    }
}
