using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Autofac;
using Microsoft.EntityFrameworkCore;

namespace api;

using db;
using core.logging;
using core.webSockets;
using NoData;

public class Startup
{
	public const string ApiPrefix = "api";

	public Startup(IConfiguration configuration)
	{
		Configuration = configuration;
	}

	public IConfiguration Configuration { get; }

	// This method gets called by the runtime. Use this method to add services to the container.
	public void ConfigureServices(IServiceCollection services)
	{
		var sql = Configuration["sql"];
		services.AddHttpContextAccessor();
		services.AddDbContext<db.DataContext>(options => options.UseNpgsql(sql));
		services.AddControllers();
		services.AddOptions();
		services.AddNoData();
		services.AddSwaggerGen(c =>
		{
			c.SwaggerDoc("v1", new OpenApiInfo { Title = "api", Version = "v1" });
			//if (!Program.IsDevelopment)
			//{
				//c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
				//{
					//Name = "Authorization",
					//Description = "Please enter a valid token",
					//In = ParameterLocation.Header,
					//Type = SecuritySchemeType.ApiKey,
				//});
				//c.AddSecurityRequirement(new OpenApiSecurityRequirement {
					//{
						//new OpenApiSecurityScheme {
							//Reference = new OpenApiReference{ Type = ReferenceType.SecurityScheme, Id = "Bearer" }
						//},
						//new string[] {}
					//}
				//});
			//}
	   });
		services.AddHostedService<DbService>();

		//if(!Program.IsDevelopment)
		//{
		//services.AddAuthentication().AddJwtBearer(x =>
		//{
		//x.Authority = Configuration.GetValue<string>("OpenIdConnect:Authority");
		//x.Audience = "account";
		//});
		//services.AddAuthorization(o =>
		//{
		//o.DefaultPolicy = new AuthorizationPolicyBuilder()
		//.RequireAuthenticatedUser()
		//.RequireClaim("name")
		//.Build();
		//});
		//}
		//else
		services.AddSingleton<IAuthorizationHandler, AllowAnonymousAuth>();

		services.AddCors(options => options.AddDefaultPolicy(policy => policy.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin()));
	}

	public void ConfigureContainer(Autofac.ContainerBuilder builder)
	{
		builder.RegisterGeneric(typeof(Controllers.CrudOperations<>));
		builder.RegisterType<DbService>();
		builder.RegisterModule(new LoggerModule(Configuration));
		builder.RegisterModule(new WebSocketModule(Configuration));
	}

	public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
	{
		if (env.IsDevelopment())
			app.UseDeveloperExceptionPage();

		app.UseSwagger(c => c.RouteTemplate = ApiPrefix + "/swagger/{documentname}/swagger.json");
		app.UseSwaggerUI(c =>
		{
			c.SwaggerEndpoint($"/{ApiPrefix}/swagger/v1/swagger.json", "api v1");
			c.RoutePrefix = $"{ApiPrefix}/swagger";
		});
		app.UsePathBase($"/{ApiPrefix}");

		app.UseRouting();
		app.UseCors();

		app.UseAuthentication();
		app.UseAuthorization();
		app.UseEndpoints(endpoints => endpoints.MapControllers());
	}
}
