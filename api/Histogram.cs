using System;

public class Histogram<T> where T : IEquatable<T>
{
	public Dictionary<T, int> Data = new();

	public Histogram()
	{
	}
	public Histogram(T value)
	{
		Add(value);
	}

	public Histogram(IEnumerable<T> values)
	{
		Add(values);
	}

	public Histogram<T> Add(T value)
	{
		Data.TryAdd(value, 0);
		Data[value] += 1;
		return this;
	}

	public Histogram<T> Add(IEnumerable<T> values)
	{
		foreach (var value in values)
			Add(value);
		return this;
	}

	public static Histogram<T> Create(IEnumerable<T> values) => new Histogram<T>(values);
}
