using System.IO;
using api.db;
using api.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NoData;

namespace api.Controllers;

using Entity = api.Entities.Monster;

[ApiController]
[Route("[controller]")]
public class MonsterController : ControllerBase
{
    private readonly DataContext context;
    private readonly CrudOperations<Entity> crud;
    private readonly ILogger logger;

	public MonsterController(DataContext context, CrudOperations<Entity> crud, ILogger<MonsterController> logger)
	{
        this.context = context;
        this.crud = crud;
        this.logger = logger;
	}

	[HttpGet]
	public ActionResult GetMany(INoData<Monster> nodata)
	{
		return Ok(nodata.Load(context.Monsters.AsNoTracking()).BuildQueryable());
	}

	[HttpGet("{id}")]
	public Task<Entity> GetSingle([FromRoute] int id) => crud.GetSingle(id);

	[HttpPost]
	public Task<Entity> Post([FromBody] Entity entity) => crud.Post(entity);

	[HttpDelete("{id}")]
	public Task<Entity> Delete(int id) => crud.Delete(id);

	[HttpPut("update-many")]
	public Task<ActionResult<List<Entity>>> PutMany([FromBody] List<Entity> entities) => crud.PutMany(entities);

	[HttpPut]
    //public async Task<ActionResult<Entity>> Put(Entity entity)
    public async Task<ActionResult> Put([FromServices] IHttpContextAccessor httpContext)
    {
		using var bodyReader = new StreamReader(httpContext.HttpContext.Request.Body);
		var bodyString = await bodyReader.ReadToEndAsync();
		var monster = Newtonsoft.Json.JsonConvert.DeserializeObject<Monster>(bodyString);

		if (!await context.Monsters.AnyAsync(x => x.Id == monster.Id))
			return new NotFoundObjectResult(monster);
		context.Update(monster);
		await context.SaveChangesAsync();
		return new OkObjectResult(monster);
    }
}
