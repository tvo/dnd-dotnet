using Microsoft.AspNetCore.Mvc;

namespace api.Controllers;

using Entity = api.Entities.Spell;

[ApiController]
[Route("[controller]")]
public class SpellController : ControllerBase
{
	private readonly CrudOperations<Entity> crud;
	private readonly ILogger logger;

	public SpellController(CrudOperations<Entity> crud, ILogger<SpellController> logger)
	{
		this.crud = crud;
		this.logger = logger;
	}

	[HttpGet]
	public IEnumerable<Entity> GetMany() => crud.GetMany();

	[HttpGet("{id}")]
	public Task<Entity> GetSingle([FromRoute] int id) => crud.GetSingle(id);

	[HttpPost]
	public Task<Entity> Post([FromBody] Entity entity) => crud.Post(entity);

	[HttpDelete("{id}")]
	public Task<Entity> Delete(int id) => crud.Delete(id);

	[HttpPut("update-many")]
	public Task<ActionResult<List<Entity>>> PutMany([FromBody] List<Entity> entities) => crud.PutMany(entities);

	[HttpPut]
	public async Task<ActionResult<Entity>> Put([FromBody] Entity entity)
	{
		return await crud.Put(entity);
	}
}
