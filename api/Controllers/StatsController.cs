using System;
using System.Data;
using api.db;
using api.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace api.Controllers;

[ApiController]
[Route("[controller]")]
public class StatsController : ControllerBase
{
    private readonly DataContext context;
    private readonly ILogger logger;

	public StatsController(DataContext context, ILogger<StatsController> logger)
	{
        this.context = context;
        this.logger = logger;
	}

	[HttpGet("monster")]
	public async Task<ActionResult> GetStats()
	{
		var monsterStats = await context.Monsters.AsNoTracking()
			.Where(x => x.Stats.PrimaryStats != null)
			.Where(x => x.Stats.SavingThrowBonus != null)
			.Select(x => new {
				x.CR,
				x.Stats.Ac,
				x.Stats.Hp,
				x.Stats.SavingThrowBonus,
				x.Stats.PrimaryStats,
			}).ToListAsync();
		var processHist = (IEnumerable<int> source) => {
			return new Histogram<int>(source).Data
				.Select(x => new {v = x.Key, c = x.Value})
				.OrderBy(x => x.v)
				.ToList();
		};
		var result = monsterStats.GroupBy(x => x.CR).Select(g => new {
			CR = g.Key,
			Ac = processHist(g.Select(x => x.Ac)),
			Hp = processHist(g.Select(x => x.Hp)),
			Con = processHist(g.Select(x => x.PrimaryStats.Con)),
			Str = processHist(g.Select(x => x.PrimaryStats.Str)),
			Dex = processHist(g.Select(x => x.PrimaryStats.Dex)),
			Int = processHist(g.Select(x => x.PrimaryStats.Int)),
			Wis = processHist(g.Select(x => x.PrimaryStats.Wis)),
			Cha = processHist(g.Select(x => x.PrimaryStats.Cha)),
			Con_SB = processHist(g.Select(x => x.SavingThrowBonus.Con)),
			Str_SB = processHist(g.Select(x => x.SavingThrowBonus.Str)),
			Dex_SB = processHist(g.Select(x => x.SavingThrowBonus.Dex)),
			Int_SB = processHist(g.Select(x => x.SavingThrowBonus.Int)),
			Wis_SB = processHist(g.Select(x => x.SavingThrowBonus.Wis)),
			Cha_SB = processHist(g.Select(x => x.SavingThrowBonus.Cha)),
		}).ToList();
		return Ok(result);
	}
}
