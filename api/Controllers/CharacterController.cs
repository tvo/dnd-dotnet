using Microsoft.AspNetCore.Mvc;
using api.db;

namespace api.Controllers;

using Entity = api.Entities.Character;

[ApiController]
[Route("[controller]")]
[Authorize]
public class CharacterController : ControllerBase
{
    private readonly CrudOperations<Entity> crud;
    private readonly DataContext context;

    public CharacterController(CrudOperations<Entity> crud, DataContext context)
    {
        this.crud = crud;
        this.context = context;
    }

    [HttpGet]
    public IEnumerable<Entity> GetMany() => crud.GetMany();

    [HttpGet("{id}")]
    public Task<Entity> GetSingle([FromRoute] int id) => crud.GetSingle(id);

    [HttpPost]
    public Task<Entity> Post([FromBody] Entity entity) => crud.Post(entity);

    [HttpDelete("{id}")]
    public async Task<Entity> Delete(int id)
    {
        var deleted = await crud.Delete(id);
        //var things = await context.Instances.Where(x => x.CharacterId == id).ToListAsync();
        //if (things.Any())
        //{
            //context.RemoveRange(things);
            //await context.SaveChangesAsync();
        //}
        return deleted;
    }

    //[HttpPut("update-many")]
    //public Task<ActionResult<List<Entity>>> PutMany([FromBody] List<Entity> entities) => crud.PutMany(entities);

    [HttpPut]
    public Task<ActionResult<Entity>> Put([FromBody] Entity entity) => crud.Put(entity);
}
