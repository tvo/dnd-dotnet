using Microsoft.AspNetCore.Mvc;

namespace api.Controllers;

using Entity = api.Entities.FiveEClass;

[ApiController]
[Route("[controller]")]
public class ClassController : ControllerBase
{
    private readonly CrudOperations<Entity> crud;

    public ClassController(CrudOperations<Entity> crud)
    {
        this.crud = crud;
    }

    [HttpGet]
    public IEnumerable<Entity> GetMany() => crud.GetMany();

    [HttpGet("{id}")]
    public Task<Entity> GetSingle([FromRoute] int id) => crud.GetSingle(id);

    [HttpPost]
    public Task<Entity> Post([FromBody] Entity entity) => crud.Post(entity);

    [HttpDelete("{id}")]
    public Task<Entity> Delete(int id) => crud.Delete(id);

    [HttpPut("update-many")]
    public Task<ActionResult<List<Entity>>> PutMany([FromBody] List<Entity> entities) => crud.PutMany(entities);

    [HttpPut]
    public Task<ActionResult<Entity>> Put([FromBody] Entity entity) => crud.Put(entity);
}
