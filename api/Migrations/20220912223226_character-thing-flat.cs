﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace api.Migrations
{
    public partial class characterthingflat : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CharacterThings_Characters_CharacterId",
                table: "CharacterThings");

            migrationBuilder.DropForeignKey(
                name: "FK_CharacterThings_Things_ThingId",
                table: "CharacterThings");

            migrationBuilder.DropForeignKey(
                name: "FK_Things_Categories_CategoryId",
                table: "Things");

            migrationBuilder.DropIndex(
                name: "IX_Things_CategoryId",
                table: "Things");

            migrationBuilder.DropIndex(
                name: "IX_CharacterThings_CharacterId",
                table: "CharacterThings");

            migrationBuilder.DropIndex(
                name: "IX_CharacterThings_ThingId",
                table: "CharacterThings");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Things_CategoryId",
                table: "Things",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_CharacterThings_CharacterId",
                table: "CharacterThings",
                column: "CharacterId");

            migrationBuilder.CreateIndex(
                name: "IX_CharacterThings_ThingId",
                table: "CharacterThings",
                column: "ThingId");

            migrationBuilder.AddForeignKey(
                name: "FK_CharacterThings_Characters_CharacterId",
                table: "CharacterThings",
                column: "CharacterId",
                principalTable: "Characters",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CharacterThings_Things_ThingId",
                table: "CharacterThings",
                column: "ThingId",
                principalTable: "Things",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Things_Categories_CategoryId",
                table: "Things",
                column: "CategoryId",
                principalTable: "Categories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
