﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace api.Migrations
{
    /// <inheritdoc />
    public partial class MonsterOwnedData : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Components",
                table: "Spell",
                type: "text",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Monster",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: true),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Link = table.Column<string>(type: "text", nullable: true),
                    CR = table.Column<float>(type: "real", nullable: false),
                    StatsHp = table.Column<int>(name: "Stats_Hp", type: "integer", nullable: true),
                    StatsAc = table.Column<int>(name: "Stats_Ac", type: "integer", nullable: true),
                    StatsProficiency = table.Column<int>(name: "Stats_Proficiency", type: "integer", nullable: true),
                    StatsPrimaryStatsCon = table.Column<int>(name: "Stats_PrimaryStats_Con", type: "integer", nullable: true),
                    StatsPrimaryStatsStr = table.Column<int>(name: "Stats_PrimaryStats_Str", type: "integer", nullable: true),
                    StatsPrimaryStatsDex = table.Column<int>(name: "Stats_PrimaryStats_Dex", type: "integer", nullable: true),
                    StatsPrimaryStatsInt = table.Column<int>(name: "Stats_PrimaryStats_Int", type: "integer", nullable: true),
                    StatsPrimaryStatsWis = table.Column<int>(name: "Stats_PrimaryStats_Wis", type: "integer", nullable: true),
                    StatsPrimaryStatsCha = table.Column<int>(name: "Stats_PrimaryStats_Cha", type: "integer", nullable: true),
                    StatsAbilityBonusCon = table.Column<int>(name: "Stats_AbilityBonus_Con", type: "integer", nullable: true),
                    StatsAbilityBonusStr = table.Column<int>(name: "Stats_AbilityBonus_Str", type: "integer", nullable: true),
                    StatsAbilityBonusDex = table.Column<int>(name: "Stats_AbilityBonus_Dex", type: "integer", nullable: true),
                    StatsAbilityBonusInt = table.Column<int>(name: "Stats_AbilityBonus_Int", type: "integer", nullable: true),
                    StatsAbilityBonusWis = table.Column<int>(name: "Stats_AbilityBonus_Wis", type: "integer", nullable: true),
                    StatsAbilityBonusCha = table.Column<int>(name: "Stats_AbilityBonus_Cha", type: "integer", nullable: true),
                    StatsSavingThrowBonusCon = table.Column<int>(name: "Stats_SavingThrowBonus_Con", type: "integer", nullable: true),
                    StatsSavingThrowBonusStr = table.Column<int>(name: "Stats_SavingThrowBonus_Str", type: "integer", nullable: true),
                    StatsSavingThrowBonusDex = table.Column<int>(name: "Stats_SavingThrowBonus_Dex", type: "integer", nullable: true),
                    StatsSavingThrowBonusInt = table.Column<int>(name: "Stats_SavingThrowBonus_Int", type: "integer", nullable: true),
                    StatsSavingThrowBonusWis = table.Column<int>(name: "Stats_SavingThrowBonus_Wis", type: "integer", nullable: true),
                    StatsSavingThrowBonusCha = table.Column<int>(name: "Stats_SavingThrowBonus_Cha", type: "integer", nullable: true),
                    StatsSkillBonusAthletics = table.Column<int>(name: "Stats_SkillBonus_Athletics", type: "integer", nullable: true),
                    StatsSkillBonusAcrobatics = table.Column<int>(name: "Stats_SkillBonus_Acrobatics", type: "integer", nullable: true),
                    StatsSkillBonusSlightOfHand = table.Column<int>(name: "Stats_SkillBonus_SlightOfHand", type: "integer", nullable: true),
                    StatsSkillBonusStealth = table.Column<int>(name: "Stats_SkillBonus_Stealth", type: "integer", nullable: true),
                    StatsSkillBonusArcana = table.Column<int>(name: "Stats_SkillBonus_Arcana", type: "integer", nullable: true),
                    StatsSkillBonusHistory = table.Column<int>(name: "Stats_SkillBonus_History", type: "integer", nullable: true),
                    StatsSkillBonusInvestigation = table.Column<int>(name: "Stats_SkillBonus_Investigation", type: "integer", nullable: true),
                    StatsSkillBonusNature = table.Column<int>(name: "Stats_SkillBonus_Nature", type: "integer", nullable: true),
                    StatsSkillBonusReligion = table.Column<int>(name: "Stats_SkillBonus_Religion", type: "integer", nullable: true),
                    StatsSkillBonusAnimalHandling = table.Column<int>(name: "Stats_SkillBonus_AnimalHandling", type: "integer", nullable: true),
                    StatsSkillBonusInsight = table.Column<int>(name: "Stats_SkillBonus_Insight", type: "integer", nullable: true),
                    StatsSkillBonusMedicine = table.Column<int>(name: "Stats_SkillBonus_Medicine", type: "integer", nullable: true),
                    StatsSkillBonusPerception = table.Column<int>(name: "Stats_SkillBonus_Perception", type: "integer", nullable: true),
                    StatsSkillBonusSurvival = table.Column<int>(name: "Stats_SkillBonus_Survival", type: "integer", nullable: true),
                    StatsSkillBonusDeception = table.Column<int>(name: "Stats_SkillBonus_Deception", type: "integer", nullable: true),
                    StatsSkillBonusIntimidation = table.Column<int>(name: "Stats_SkillBonus_Intimidation", type: "integer", nullable: true),
                    StatsSkillBonusPerformance = table.Column<int>(name: "Stats_SkillBonus_Performance", type: "integer", nullable: true),
                    StatsSkillBonusPersuasion = table.Column<int>(name: "Stats_SkillBonus_Persuasion", type: "integer", nullable: true),
                    StatsSpellCastingStat = table.Column<string>(name: "Stats_SpellCastingStat", type: "text", nullable: true),
                    CreatureStatsInitiative = table.Column<int>(name: "CreatureStats_Initiative", type: "integer", nullable: true),
                    CreatureStatsProficiency = table.Column<int>(name: "CreatureStats_Proficiency", type: "integer", nullable: true),
                    CreatureStatsSpeedWalking = table.Column<int>(name: "CreatureStats_Speed_Walking", type: "integer", nullable: true),
                    CreatureStatsSpeedSwimming = table.Column<int>(name: "CreatureStats_Speed_Swimming", type: "integer", nullable: true),
                    CreatureStatsSpeedFlying = table.Column<int>(name: "CreatureStats_Speed_Flying", type: "integer", nullable: true),
                    CreatureStatsSpeedClimbing = table.Column<int>(name: "CreatureStats_Speed_Climbing", type: "integer", nullable: true),
                    CreatureStatsSpeedBurrowing = table.Column<int>(name: "CreatureStats_Speed_Burrowing", type: "integer", nullable: true),
                    CreatureStatsVisionDarkVision = table.Column<int>(name: "CreatureStats_Vision_DarkVision", type: "integer", nullable: true),
                    CreatureStatsVisionBlindSight = table.Column<int>(name: "CreatureStats_Vision_BlindSight", type: "integer", nullable: true),
                    CreatureStatsVisionTrueSight = table.Column<int>(name: "CreatureStats_Vision_TrueSight", type: "integer", nullable: true),
                    CreatureStatsSize = table.Column<string>(name: "CreatureStats_Size", type: "text", nullable: true),
                    CreatureStatsType = table.Column<string>(name: "CreatureStats_Type", type: "text", nullable: true),
                    CreatureStatsConditionImmunities = table.Column<int[]>(name: "CreatureStats_ConditionImmunities", type: "integer[]", nullable: true),
                    AttackDescription = table.Column<string>(type: "text", nullable: true),
                    LegendaryDescription = table.Column<string>(type: "text", nullable: true),
                    LegendaryActions = table.Column<int>(type: "integer", nullable: false),
                    SourceBook = table.Column<string>(type: "text", nullable: true),
                    ActionsCapacity = table.Column<int>(name: "Actions_Capacity", type: "integer", nullable: true),
                    LanguagesCapacity = table.Column<int>(name: "Languages_Capacity", type: "integer", nullable: true),
                    Environment = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Monster", x => x.Id);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Monster");

            migrationBuilder.DropColumn(
                name: "Components",
                table: "Spell");
        }
    }
}
