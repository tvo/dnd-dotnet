﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace api.Migrations
{
    /// <inheritdoc />
    public partial class MonsterUpdateMigration : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Monster",
                table: "Monster");

            migrationBuilder.RenameTable(
                name: "Monster",
                newName: "Monsters");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Monsters",
                table: "Monsters",
                column: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Monsters",
                table: "Monsters");

            migrationBuilder.RenameTable(
                name: "Monsters",
                newName: "Monster");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Monster",
                table: "Monster",
                column: "Id");
        }
    }
}
