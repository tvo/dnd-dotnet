TTRPG character builder and stat analyzer.


Glossary:

Templates
This is primarily a json schema validator and schema entity. This is intended to be used to unify entities which have similar functionality. For example the human race is similar to the elf race, thus they should have the same schema to make accessing these races similar from the UI perspective.

Categories
Associates multiple things. This is used to separate unrelated things from eachother while building a character. For example you don't want to see 3e feats while building a 5e character.

Tags
Tags are a mechanism designed for the ui so that the user can easilly associate text in the rules to a section in the rules. For example if a spell causes the "prone" condition, the text prone could link to the condition which gives a full explaination of the condition's effects.

Thing
This is a section of the rules which you can potentially associate to a character. For example a stat block here would define the 6 primary stats, con str dex etc...(for 5th), that a character would have. It would not have the stats associated with a character - so the stat numbers would be blank. Another example of what this covers is wizard class, or the human race.

Instance
This is an owned thing. When you create a character - all the information is stored here. The character will have a stat block - skills, proficiencies, ac calculations, feats, all that is stored here.
