docker stop pg-docker || true
docker rm pg-docker || true
docker rmi pg-docker || true

sleep 2

docker run -d --name pg-docker \
	-p 5432:5432 \
	-e POSTGRES_PASSWORD=docker \
	-v pgdata:/var/lib/postgresql/data \
	postgres

